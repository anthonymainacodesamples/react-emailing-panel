/**
 * Created by AnthonyMaina on 10/7/17.
 */

const mongoose = require('mongoose');
const { Schema } = mongoose;

const recipientSchema = new Schema ({
    email: String,
    responded: {type: Boolean, default: false}
});

module.exports = recipientSchema;