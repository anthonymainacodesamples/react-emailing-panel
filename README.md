# React Emailing Panel

Code repository by Anthony Maina.

### Languages ###
Javascript (ES6)

### Quick summary 
This is a personal project and deep dive into react and redux to build a MERN stack web app that integrates into a 3rd party emailing api (Sendgrid).

### Front End ###
The front end is built entirely in react and material design.

### Back End ###
The back end is a nodejs/express server.

### Version ###
1.0

### How do I get set up? ###

To get set up run npm run dev in the main directory.
