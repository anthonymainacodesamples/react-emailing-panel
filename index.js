/**
 * Created by AnthonyMaina on 9/27/17.
 */

//Const declarations
const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./config/keys');
const bodyParser = require('body-parser');


//Require statements
require('./models/User');
require('./models/Survey');
require('./services/passport');


//Connect mongoose instance to our mongodb
mongoose.connect(keys.mongoURI);

//Express route handlers
const app = express();

app.use(bodyParser.json());

app.use(
  cookieSession({
      //30 days
      maxAge: 30*24*60*60*1000,
      keys: [keys.cookieKey]
  })
);

app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/billingRoutes')(app);
require('./routes/surveyRoutes')(app);

if (process.env.NODE_ENV === 'production') {
    // Express will serve up production assets
    //Like main.js or main.css!
    app.use(express.static('./client/build'));

    //Express will serve up index.html if it doesn't recognize the route
    const path = require('path');
    app.get('*', (req,res) => {
       res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}



//Port listening
const PORT = process.env.PORT || 5000;
app.listen(PORT);
