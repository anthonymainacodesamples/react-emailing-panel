/**
 * Created by AnthonyMaina on 10/6/17.
 */
import React,{ Component } from 'react';
// import { connect } from 'react-redux';
// import { bindActionCreators}  from 'redux';


class SearchBar extends Component {
    render() {
        return(
            <form className="input-group">
                <input
                    className="form-control"
                    placeholder="Type your search here and press enter"
                    />
                <span className="input-group-btn">
                  <button type="submit" className="btn btn-secondary"> Search</button>
              </span>
            </form>
        );
    }
}

export default SearchBar;