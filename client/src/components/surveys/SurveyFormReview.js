/**
 * Created by AnthonyMaina on 10/11/17.
 */

import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import formFields from './formFields';
import * as actions from '../../actions';

const SurveyReviewForm = ({ onCancel, formValues, submitSurvey, history }) => {

    const reviewFields = _.map(formFields, ({ name,label }) =>{
       return (
        <div key={name}>
            <label>{label}</label>
            <div>
                { formValues[name] }
            </div>
        </div>
       ) ;
    });

    return(
        <div>
            <h5>Survey Review manenoz!</h5>
            {reviewFields}
            <button
                className="yellow darken-3 white-text btn-flat"
                onClick ={onCancel}
            >
                Back
            </button>
            <button
                className="right btn-flat green white-text"
                onClick={() => submitSurvey(formValues,history)}
            >
                Send Survey
                <i className="material-icons right"/> email
            </button>
        </div>

    );
};
function mapStateToProps(state) {
    return {
        formValues: state.form.surveyForm.values
    };
}



export default connect(mapStateToProps, actions)(withRouter(SurveyReviewForm));