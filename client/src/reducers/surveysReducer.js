/**
 * Created by AnthonyMaina on 10/13/17.
 */

import { FETCH_SURVEYS } from '../actions/types';

export default function(state = [],action) {

    switch(action.types) {
        case FETCH_SURVEYS:
            return action.payload;
        default:
            return state;
    }
};