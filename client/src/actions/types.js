/**
 * Created by AnthonyMaina on 10/5/17.
 */

export const FETCH_USER = 'fetch_user';
export const FETCH_SURVEYS = 'fetch_surveys';