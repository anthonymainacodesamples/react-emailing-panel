/**
 * Created by AnthonyMaina on 10/7/17.
 */

module.exports = (req,res,next) => {
    if(req.user.credits < 1) {
        return res.status(403).send({ error: 'Not enpugh credits!' });
    }

    next();
};