/**
 * Created by AnthonyMaina on 9/28/17.
 */

if(process.env.NODE_ENV === 'production') {
    module.exports =  require('./prod');
} else {
   module.exports =  require('./dev');
}